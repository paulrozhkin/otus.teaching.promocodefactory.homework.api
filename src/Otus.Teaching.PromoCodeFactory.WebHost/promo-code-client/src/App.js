import './App.css';
import {Container, Row, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import axios from "axios";
import {HubConnectionBuilder} from '@microsoft/signalr';

function App() {
    const [customers, setCustomers] = useState([])
    const [connection, setConnection] = useState(null);

    function updateCustomers() {
        axios.get('https://localhost:5001/api/v1/Customers')
            .then(customersResponse => {
                setCustomers(customersResponse.data)
            })
    }

    useEffect(() => {
        const newConnection = new HubConnectionBuilder()
            .withUrl('https://localhost:5001/hubs/customers', 2)
            .withAutomaticReconnect()
            .build();

        setConnection(newConnection);
        updateCustomers()
    }, [])

    useEffect(() => {
        if (connection) {
            connection.start()
                .then(result => {
                    console.log('SignalR Connected!');

                    connection.on('RefreshCustomers', message => {
                        console.log('SignalR customers updated!');
                        updateCustomers()
                    });
                })
                .catch(e => console.log('Connection failed: ', e));
        }
    }, [connection]);

    return (
        <div className="App">
            <Container>
                <Row>
                    <h1 className="mt-5 text-center">Список покупателей (SignalR updates)</h1>
                </Row>
                <Row>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        {customers.map(customer =>
                            <tr key={customer.id}>
                                <td>{customer.id}</td>
                                <td>{customer.firstName}</td>
                                <td>{customer.lastName}</td>
                                <td>{customer.email}</td>
                            </tr>
                        )}
                        </tbody>
                    </Table>
                </Row>
            </Container>
        </div>
    );
}

export default App;
