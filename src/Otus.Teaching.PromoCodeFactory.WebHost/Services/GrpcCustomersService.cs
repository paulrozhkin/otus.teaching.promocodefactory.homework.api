﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcCustomers;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using DomainCustomer = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Customer;
using DomainPreference = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Preference;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class GrpcCustomersService : Customers.CustomersBase
    {
        private readonly IRepository<DomainCustomer> _customerRepository;
        private readonly IRepository<DomainPreference> _preferenceRepository;
        private readonly IHubContext<CustomersHub> _customersHub;

        public GrpcCustomersService(IRepository<DomainCustomer> customerRepository,
            IRepository<DomainPreference> preferenceRepository,
            IHubContext<CustomersHub> customersHub)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customersHub = customersHub;
        }

        public override async Task<GetCustomersReply> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersShortResponse = customers.Select(x => new CustomerShort()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var response = new GetCustomersReply();
            response.Customers.AddRange(customersShortResponse);

            return response;
        }

        public override async Task<GetCustomerReply> GetCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, nameof(request.Id)));
            }

            var customer = await _customerRepository.GetByIdAsync(id);

            return new GetCustomerReply()
            {
                Customer = MapModelToDto(customer)
            };
        }

        public override async Task<CreateCustomerReply> CreateCustomer(CreateCustomerRequest request,
            ServerCallContext context)
        {
            var preferenceIdsGuid = request.Customer.PreferenceIds.Select(Guid.Parse).ToList();

            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferenceIdsGuid);

            var customer = CustomerMapper.MapFromModel(request.Customer, preferences);

            await _customerRepository.AddAsync(customer);
            await _customersHub.Clients.All.SendAsync("RefreshCustomers");

            return new CreateCustomerReply()
            {
                Customer = MapModelToDto(customer)
            };
        }

        public override async Task<Empty> EditCustomers(EditCustomersRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, nameof(request.Id)));
            }


            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, nameof(request.Id)));
            }

            var preferenceIdsGuid = request.Customer.PreferenceIds.Select(Guid.Parse).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIdsGuid);

            CustomerMapper.MapFromModel(request.Customer, preferences, customer);

            await _customerRepository.UpdateAsync(customer);
            await _customersHub.Clients.All.SendAsync("RefreshCustomers");

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(DeleteCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, nameof(request.Id)));
            }

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, nameof(request.Id)));
            }

            await _customerRepository.DeleteAsync(customer);
            await _customersHub.Clients.All.SendAsync("RefreshCustomers");

            return new Empty();
        }

        private Customer MapModelToDto(DomainCustomer customer)
        {
            var customerDto = new Customer()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            var preferences = customer.Preferences.Select(x => new Preference()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            });

            customerDto.Preferences.AddRange(preferences);
            return customerDto;
        }

    }
}